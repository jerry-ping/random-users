import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { 
  ChakraProvider,
  Box,
  Flex
} from "@chakra-ui/react";
import "./App.css";
import UserList from "./pages/UserList";
import customTheme from "./styles/customTheme";

function App() {
  return (
    <div className="App">
      <ChakraProvider theme={customTheme}>
        <Flex w="full" minH="100vh" justifyContent="center">
          <Box w="full" maxW={{ "base": "100%", "md": "1480px" }} p={{ base: "20px", md: "40px" }}>
            <Router>
              <Switch>
                <Route exact path="/">
                  <UserList />
                </Route>
                <Route exact path="*">
                  <Redirect to="/" />
                </Route>
              </Switch>
            </Router>
          </Box>
        </Flex>
      </ChakraProvider>
    </div>
  );
}

export default App;
