async function fetchUserData(page: number) {
  try {
    const respone = await fetch(`https://randomuser.me/api/?page=${page}&results=20&seed=abc`);
    const data = await respone.json();
  
    return data.results;
  } catch (e) {
    console.error(e);
  }
}

export {
  fetchUserData,
}