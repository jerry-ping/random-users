import React from "react";
import {
  Text,
  Box,
  Flex,
  Button
} from "@chakra-ui/react";

type ContactInfoModalProps = {
  phone: string,
  email: string,
  name: string,
  onClose: () => void
}

const ContactInfoModal = ({ name, phone, email, onClose }: ContactInfoModalProps) => {
  return (
    <Flex position="fixed" w="100vw" h="100vh" justifyContent="center" alignItems="center" top="0" left="0" zIndex="10000">
      <Box w="full" h="full" position="absolute" bg="rgba(120, 120, 120, 0.5)" />
      <Box w="400px" h="300px" bg="white" zIndex="10001" borderRadius="20px" border="10px solid grey" p="50px">
      <Text fontSize="xl">{name}</Text>
        <Text fontSize="lg" mt="10px">Phone Number: {phone}</Text>
        <Text fontSize="lg" mt="10px">Email: {email}</Text>
        <Button onClick={onClose} colorScheme="blue" fontWeight="bold" mt="40px" w="180px">Ok</Button>
      </Box>
    </Flex>
  );
}

export default React.memo(ContactInfoModal);