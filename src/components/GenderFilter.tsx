import React from "react";
import {
  Select
} from "@chakra-ui/react";

type GenderFilterProps = {
  onChange: (e: any) => void;
}

const GenderFilter = ({ onChange }: GenderFilterProps) => {
  return (
    <Select onChange={onChange} w="300px" maxW="full">
      <option value="all">No Filter</option>
      <option value="male">Male</option>
      <option value="female">Female</option>
    </Select>
  );
}

export default React.memo(GenderFilter);