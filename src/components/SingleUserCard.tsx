import React from "react";
import {
  Flex,
  Box,
  Image,
  Text,
  Button
} from "@chakra-ui/react";
import { CloseIcon } from '@chakra-ui/icons'

type Name = {
  title: string;
  first: string;
  last: string;
};

type SingleUserCardProps = {
  name: Name,
  gender: string,
  picture: string,
  onClose: () => void,
  onConnect: () => void
}

const SingleUserCard = ({ onConnect, name, gender, picture, onClose }: SingleUserCardProps) => {
  return (
    <Box position="relative">
      <CloseIcon onClick={onClose} className="close-icon" display="none" cursor="pointer" w={10} h={10} bg="#304654" color="white" borderRadius="50%" p="10px" position="absolute" right="10px" top="10px"/>
      <Box w="full" h="120px" color="white" textAlign="left" bgGradient="radial(#3A7399, #1A4B72)">
      </Box>
      <Flex justifyContent="center">
        <Box w="150px" h="150px" mt="-75px" borderRadius="50%" border="1px solid #ccc">
          <Image borderRadius="50%" border="10px solid white" w="full" h="full" src={picture} alt="Profile Image" />
        </Box>
      </Flex>
      <Text fontSize="xl" fontWeight="bold" mt="10px">{name.first} {name.last}</Text>
      <Text fontSize="md" color="#888" mt="10px">{gender}</Text>
      <Button onClick={onConnect} colorScheme="blue" variant="outline" fontWeight="bold" mt="40px" w="180px">Connect</Button>
    </Box>
  );
}

export default React.memo(SingleUserCard);