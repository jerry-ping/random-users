import React, { useCallback, useState, useEffect, useRef } from "react";
import {
  Flex,
  Box,
  Spinner
} from "@chakra-ui/react";
import * as api from "../api/api";
import SingleUserCard from "../components/SingleUserCard";
import ContactInfoModal from "../components/ContactInfoModal";

type UserCardListProps = {
  genderFilter: string
}

const UserCardList = ({ genderFilter }: UserCardListProps) => {
  const [userData, setUserData] = useState<any[]>([]);
  const [filteredUserData, setFilteredUserData] = useState<any[]>([]);
  const [pageNum, setPageNum] = useState<number>(1);
  const [isModalOpen, setModalOpen] = useState<boolean>(false);
  const [connectedUser, setConnectedUser] = useState<any>({});
  const [isLoading, setLoading] = useState<boolean>(true);
  const observer: any = useRef();

  const fetchData = useCallback(async () => {
    setLoading(true);
    const result: any[] = await api.fetchUserData(pageNum);
    setPageNum(pageNum + 1)
    setUserData(_userData => [..._userData, ...result]);
    setLoading(false);
  }, [pageNum]);

  useEffect(() => {
    if (pageNum === 1) fetchData();
  }, [fetchData, pageNum]);

  useEffect(() => {
    setFilteredUserData(genderFilter === "all" ? userData : userData.filter(data => data.gender === genderFilter));
  }, [genderFilter, userData]);

  const handleClose = useCallback(async (uuid) => {
    setUserData(_userData => _userData.filter((data) => uuid !== data.login.uuid));
  }, []);

  const handleConnect = useCallback(async (id) => {
    setConnectedUser(filteredUserData[id]);
    setModalOpen(true);
  }, [filteredUserData]);

  const loadingElementRef = useCallback(
    (node) => {
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting) {
          if (isLoading) return;
          fetchData();
        }
      });
      if (node) observer.current.observe(node);
    },
    [isLoading, fetchData]
  );

  return (
    <>
      <Flex wrap="wrap" justifyContent={{ base: 'space-around', xl: 'flex-start' }}>
        {filteredUserData
          .map((data, index) => (
            <Box className="card" key={data.login.uuid}>
              <SingleUserCard onConnect={() => handleConnect(index)} onClose={() => handleClose(data.login.uuid)} picture={data.picture.medium} name={data.name} gender={data.gender} />
            </Box>
        ))}
        <Box w="full" h="20px" ref={loadingElementRef}>
          {isLoading && <Spinner size='xl' color='blue.500' />}
        </Box>
      </Flex>
      {isModalOpen && <ContactInfoModal onClose={() => setModalOpen(false)} name={`${connectedUser.name.first} ${connectedUser.name.last}`} phone={connectedUser.phone} email={connectedUser.email} />}
    </>
  );
}

export default React.memo(UserCardList);