import React, { useState } from "react";
import { 
  Box,
  Flex
} from "@chakra-ui/react";
import GenderFilter from "../components/GenderFilter";
import UserCardList from "../components/UserCardList";

function UserList() {
  const [genderFilter, setGenderFilter] = useState("all");

  return (
    <>
      <Flex justifyContent="center">
        <GenderFilter onChange={(e) => { setGenderFilter(e.target.value)} } />
      </Flex>
      <Box mt="10px">
        <UserCardList genderFilter={genderFilter} />
      </Box>
    </>
  );
}

export default UserList;