import { extendTheme } from "@chakra-ui/react";

const customTheme = extendTheme({
  styles: {
    global: (props: any) => ({
      boxSizing: "border-box",
      ".card": {
        width: "260px",
        height: "380px",
        margin: "10px",
        overflow: "hidden",
        [`@media screen and (max-width: ${props.theme.breakpoints.sm})`]: {
          width: "100%",
          height: "auto"
        },
        border: "1px solid #aaa",
        ":hover": {
          boxShadow: "5px 5px 10px #555",
          ".close-icon": {
            display: "block"
          }
        }
      }
    }),
  },
});

export default customTheme;
